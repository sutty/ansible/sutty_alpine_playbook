# frozen_string_literal: true

require 'yaml'

# Represents Ansible inventory
module AnsibleInventory
  # @return [Hash]
  def self.inventory
    @@inventory ||= YAML.safe_load(File.read('./inventory.yml'))
  end

  # @return [Array]
  def self.groups
    @@groups ||= inventory.keys
  end

  # @return [nil]
  def self.read
    groups.each do |group|
      inventory[group]['hosts'] ||= {}
      inventory[group]['hosts'].tap do |hosts|
        hosts.each_pair do |host, _vars|
          hosts[host] = host_vars(host)
        end
      end
    end

    nil
  end

  def self.host_vars(host)
    file = File.join('host_vars', "#{host}.yml")

    if File.exist? file
      YAML.safe_load(File.read(file))
    else
      puts "Warning: #{file} doesn't exist"
      {}
    end
  end
end
