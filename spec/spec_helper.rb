# frozen_string_literal: true

require 'serverspec'
require 'net/ssh'

host = ENV['TARGET_HOST']
options = Net::SSH::Config.for(host)

options[:user] ||= 'root'
options[:forward_agent] = true
options[:auth_methods] = %w[publickey]

set :backend, :ssh
set :host, options[:host_name] || host
set :ssh_options, options
set :disable_sudo, true
set :env, LANG: 'C.UTF-8', LC_MESSAGES: 'C'
