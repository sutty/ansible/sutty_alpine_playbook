# frozen_string_literal: true

require 'spec_helper'
require 'ansible_inventory'

host_name = ENV['TARGET_HOSTNAME']
host_vars = AnsibleInventory.host_vars host_name

describe default_gateway do
  its(:ipaddress) { should eq host_vars['gateway'] }
  its(:interface) { should eq 'eth0' }
end
