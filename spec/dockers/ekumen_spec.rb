# frozen_string_literal: true

require 'spec_helper'

describe package('tinc') do
  it { should be_installed }
end

describe service('tincd') do
  it { should be_enabled }
  it { should be_running }
end

describe port(65_000) do
  it { should be_listening.with('tcp') }
  it { should be_listening.with('udp') }
end

describe interface('ekumen') do
  it { should exist }
  its(:ipv6_address) { should match /\Afd00:acab::/ }
end
