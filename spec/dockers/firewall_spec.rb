# frozen_string_literal: true

require 'spec_helper'
require 'ansible_inventory'

host_name = ENV['TARGET_HOSTNAME']
host_vars = AnsibleInventory.host_vars host_name

describe service('ipset') do
  it { should be_enabled }
  it { should be_running }
end

describe service('iptables') do
  it { should be_enabled }
  it { should be_running }
end

describe service('ip6tables') do
  it { should be_enabled }
  it { should be_running }
end
