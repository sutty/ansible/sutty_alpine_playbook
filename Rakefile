# frozen_string_literal: true

require 'rake'
require 'rspec/core/rake_task'
require_relative 'spec/ansible_inventory'
require 'pry'

task spec: 'spec:all'
task default: :spec

AnsibleInventory.read

namespace :spec do
  desc 'Run serverspec'
  task all: (AnsibleInventory.groups.map do |group|
    "#{group}:all"
  end)

  AnsibleInventory.groups.each do |group|
    short_names = AnsibleInventory.inventory[group]['hosts'].map do |host, vars|
      {
        vars['ekumen'] => {
          host: vars['ansible_host'],
          hostname: host
        }
      }
    end.inject(&:merge) || {}

    namespace group.to_sym do
      desc "Run serverspec on #{group}"
      task all: short_names.keys

      short_names.each_key do |name|
        desc "Run serverspec on #{group}:#{name}"
        RSpec::Core::RakeTask.new(name) do |t|
          ENV['TARGET_HOST'] = short_names[name][:host]
          ENV['TARGET_HOSTNAME'] = short_names[name][:hostname]

          puts "Running serverspec on #{group}:#{name}"
          t.pattern = "spec/{base,#{group},#{name}}/*_spec.rb"
        end
      end
    end
  end
end
